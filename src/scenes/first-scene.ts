import * as Phaser  from 'phaser';
import Controls from "../components/Controls";


export default class FirstScene extends Phaser.Scene {
    controls: Controls;

    constructor() {
        super('FirstScene');
    }

    preload() {
        
    }

    create() {    
        const text = this.add.text(5, 5,
            `
                Hello
                
                Breef: AI set time to self-destroy.
                Go and destroy all mainframes to stop it.

                Controls:
                AD - Rotate mech's head
                Left, Right - Rotate mech's legs
                Up - Go
                Space - Fire
                Left click - Go to fullscreen mode and play
            `, 
            {
                fontFamily: '"m3x6"',
                fontSize: '50px'
            });

        this.controls = new Controls(this.input);
    }

    update() {
        if (this.controls.isStart()) {
            this.scale.startFullscreen();
            this.scene.start('TestScene');
        }
    }
}
