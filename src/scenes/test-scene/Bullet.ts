import * as Phaser  from 'phaser';
import TestScene from './test-scene';
import { Gun } from '../../types';
import Body = Phaser.Physics.Arcade.Body;

const BULLET_SPEED = 600;

export default class Bullet extends Phaser.GameObjects.Image {
    public speed: number;
    public born: number;
    public damage: number;
    public flashKey: string;
    public flashAnimKey: string;    
    public scene: TestScene;
    constructor(scene: TestScene, x: number, y: number, texture: string) {        
        super(scene, x, y, texture);
        this.setTexture('bullet');
        this.speed = 0;
        this.born = 0;
        this.damage = 10;
        this.flashKey = 'minigun_flash';
        this.flashAnimKey = 'minigun.shoot';
    }

    fire(gun: Gun) {
        this.scene.soundShoot.play();
        this.setSize(1, 1).setScale(0.1);
        
        var flash = this.scene.add.sprite(gun.x, gun.y, this.flashKey);
        flash.setScale(0.3);
        flash.setOrigin(0.2, 0.5);
        flash.rotation = gun.rotation;
        flash.play(this.flashAnimKey);
        this.setPosition(gun.x, gun.y);

        this.rotation = gun.rotation;
        this.setPosition(gun.x, gun.y);
        
        this.scene.physics.velocityFromRotation(this.rotation, BULLET_SPEED, (<Body>this.body).velocity);

        this.born = 0;
    }

    update (time: number, delta: number) {
        this.x += this.speed * delta;

        this.born += delta;

        if (this.born > 1000)
        {
            this.destroy();
        }
    }

    destroy () {
        this.setActive(false);
        this.setVisible(false);
        (<Body>this.body).checkCollision.none = true;
        this.scene.physics.world.disable(this);
    }

    activate() {
        this.setActive(true);
        this.setVisible(true);
        (<Body>this.body).checkCollision.none = false;
        this.scene.physics.world.enable(this);
    }
}
