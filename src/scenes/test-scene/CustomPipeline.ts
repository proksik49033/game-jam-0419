import * as Phaser  from 'phaser';

export default class CustomPipeline extends Phaser.Renderer.WebGL.Pipelines.TextureTintPipeline {
    constructor(game: Phaser.Game, shader: string) {
        super({
            game: game,
            renderer: game.renderer,
            fragShader: shader
        });
    }
}