import Unit from './Unit';
import { Gun } from '../../types';

export default class Turel extends Unit {
    constructor() {        
        super(...arguments);        
        this.play('turret.idle', true);
    }

    getWeapon(): Gun[] {
        return [this];
    }

    update(time: number, delta: number) {        
        if (this.isNear() && this.isInCamera()) {
            this.rotation = Phaser.Math.Angle.Between(this.x, this.y, this.scene.player.x, this.scene.player.y);
            this.fire(time, delta);
            this.play('turret.fire', true);
        }
        super.update(time, delta);
    }

    getMaxHealth(): number {
        return 100;
    }

    destroy() {
        if (this.scene) {
            var explosive = this.scene.add.sprite(this.x, this.y, 'explosion');
            this.scene.soundBoom.play();
            explosive.setScale(2);
            explosive.play('explosive');
        }        
        super.destroy();        
    }
}