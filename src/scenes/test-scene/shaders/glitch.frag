 precision mediump float;

    uniform sampler2D uMainSempler;
    uniform float time;
    varying vec2 outTexCoord;
    uniform vec2 u_canvas_size;

    float rand(vec2);


    void main() {
        vec2 texture_coord = outTexCoord;        

        float u_intensity = 20.0;

        float random_value = u_intensity * rand(vec2(floor(texture_coord.y * 20.0), time));

        if (random_value < 0.05) {
            gl_FragColor = texture2D(uMainSempler,
                vec2(texture_coord.x + random_value / 5.0,
                     texture_coord.y));            
        } else {
            gl_FragColor = texture2D(uMainSempler, texture_coord);
        }
    }


    float rand(vec2 seed) {
        return fract(sin(dot(seed, vec2(12.9898,78.233))) * 43758.5453123);
    }
   