import Unit from './Unit';
import * as Phaser  from 'phaser';
import TestScene from './test-scene';
import { Gun } from '../../types';
import Player from './Player';
import Bullet from './Bullet';

const SPEED = 70;

export default class Soldier extends Unit {
    private stateStack: CallableFunction[];
    public scene: TestScene;
    constructor() {
        super(...arguments);        
        this.idle = this.idle.bind(this);
        this.walkToEnemy = this.walkToEnemy.bind(this);
        this.atack = this.atack.bind(this);
        this.stateStack = [this.idle];        
    }

    getWeapon(): Gun[] {
        return [{
            x: this.x,
            y: this.y,
            rotation: this.rotation
        }];
    }

    idle() {
        this.play('soldier.idle', true);
        if (this.isNear()) {
            this.alarm();
        }
    }

    isAtackAvailable(): boolean {
        return this.distanceToPlayer() < 200;
    }

    walkToEnemy() {
        this.play('soldier.walk', true);
        this.rotation = Phaser.Math.Angle.Between(this.x, this.y, this.scene.player.x, this.scene.player.y);
        this.scene.physics.velocityFromRotation(this.rotation, SPEED, this.body.velocity);

        if (this.isAtackAvailable()) {            
            this.stateStack.push(this.atack);
            this.body.stop();
        }
    }

    atack(time: number, delta: number) {
        this.rotation = Phaser.Math.Angle.Between(this.x, this.y, this.scene.player.x, this.scene.player.y);
        this.fire(time, delta);
        this.play('soldier.fire', true);
        if (!this.isAtackAvailable()) {
            this.stateStack.pop();
        }
    }

    alarm() {
        this.stateStack.push(this.walkToEnemy);        
    }

    update(time: number, delta: number) {
        this.stateStack[this.stateStack.length - 1](time, delta);
        super.update(time, delta);
    }

    destroy() {
        if (this.scene) {
            var blood = this.scene.add.sprite(this.x, this.y, 'blood');
            blood.setScale(0.05);
            blood.play('bloodsplat');
            this.scene.soundDeath.play();
        }        
        super.destroy();
    }

    dead(player: Player, soldier: Soldier) {
        soldier.destroy();
    }

    hit(bullet: Bullet, unit: Unit) {
        super.hit(unit, bullet);
    }
}