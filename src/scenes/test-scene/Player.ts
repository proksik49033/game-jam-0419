import Zone = Phaser.GameObjects.Zone;
import DynamicTilemapLayer = Phaser.Tilemaps.DynamicTilemapLayer;
import Tile = Phaser.Tilemaps.Tile;
import Bullet from './Bullet';
import Mech from './Mech';
import fontStyles from '../fontStyles';
import TestScene from './test-scene';

const MAX_HEAT = 10000;
const CRITICAL_CONFIG = {
    electro: {
        colorIncrement: 0x008888,
        text: 'electroshock'
    },
    overHeating: {
        colorIncrement: 0x001111,
        text: 'overheating'
    }
};

export default class Player extends Mech {
    public healthDisplay;
    public heatDisplay;
    public criticalDisplay;
    public heat:number;
    public electro:number;
    public tintColor:number;
    public criticalDisplayColor:number;
    public overHeating;

    constructor(scene: TestScene, x:number, y:number) {
        super(scene, x, y);
        this.healthDisplay = scene
            .add
            .text(5, 5, 'Health: 100%', fontStyles)
            .setScrollFactor(0)
            .setDepth(100);
        this.heatDisplay = scene
            .add
            .text(5, 30, 'Heat: 0%', fontStyles)
            .setScrollFactor(0)
            .setDepth(100);
        this.criticalDisplay = scene
            .add
            .text(
                0,
                0,                
                'electro shock',
                {                    
                    ...fontStyles,
                    fontSize: 50,
                    fontWeight: 900,
                }
            )
            .setScrollFactor(0)
            .setDepth(100)            
            .setVisible(false);
        this.criticalDisplay.setPosition(
            +scene.game.config.width / 2 - this.criticalDisplay.width / 2,
            +scene.game.config.height / 2 - this.criticalDisplay.height / 2 ,
        );
        
        this.heat = 0;
        this.electro = 0;
        this.tintColor = 0xffffff;
        this.criticalDisplayColor = 0xffffff;
    }

    getCritical(): string | boolean {
        if (this.isElectro()) return 'electro';
        if (this.overHeating) return 'overHeating';
        return false;
    }

    update(time, delta) {        
        this.head.setTint(this.tintColor);
        this.criticalDisplay.setVisible(false);
        if (this.getCritical()) {
            this.criticalDisplay.setVisible(true);
            let criticalConfig = CRITICAL_CONFIG[<string>this.getCritical()];
            
            this.criticalDisplayColor += criticalConfig.colorIncrement;            
            if (this.criticalDisplayColor >= 0xffffff && !this.isElectro()) {
                this.criticalDisplayColor = 0xff0000;
            }
            this.criticalDisplay.setTint(this.criticalDisplayColor);
            this.criticalDisplay.setText(criticalConfig.text);
        }
        this.tintColor += 0x008888;
        if (this.tintColor >= 0xffffff && !this.isElectro()) {
            this.tintColor = 0xffffff;
        }
        if (!this.isElectro()) {
            super.update(time, delta);
        } else {
            this.body.stop();
            this.play('mech.stay');
            this.scene.soundWalk.stop();
            this.criticalDisplayColor += 0x008888;            
        }
        var health = this.calcHealth();
        var heat = this.calcHeat();
        this.healthDisplay.setText(`Health: ${health}%`);
        this.heatDisplay.setText(`Heat: ${heat}%`);
        if (this.overHeating) {
            this.heatDisplay.setStyle({
                color: '#880000'
            });
        } else {
            this.heatDisplay.setStyle({
                color: '#fff'
            });
        }
        if (this.heat > 0) {
            this.heat -= delta;
        } else {
            this.overHeating = false;
        }
        if (this.isElectro()) {
            this.electro -= delta;            
        }        
    }

    isElectro(): boolean {
        return this.electro > 0;
    }

    calcHealth(): number {
        var healthPercent = this.health / this.getMaxHealth() * 100;
        return Number(healthPercent.toFixed(0));
    }
    
    calcHeat(): number {
        var heatPercent = this.heat / MAX_HEAT * 100;
        return Number(heatPercent.toFixed(0));
    }

    fire(time: number, delta: number) {
        if (this.overHeating) {
            return;
        }
        if (this.scene.time.now > this.nextFire) {
            this.heat +=  160;            
            if (this.heat > MAX_HEAT) {
                this.scene.soundOverheating.play();
                this.overHeating = true;
            }
        }
        super.fire(time, delta);
    }

    hitElectro() {
        this.electro = 3000;
        var electroAnim = this.scene.add.sprite(this.x, this.y, 'electric_explosion', 3);
        electroAnim.play('explosive.electric');
        this.scene.soundElectro.play();
        this.scene.soundBoom.play();
    }

    hit (unit: Player) {        
        if (unit.mech) {
            unit = unit.mech;
        }
        if (!unit.isElectro()) {
            unit.tintColor = 0xff0000;
        }
        super.hit(...arguments);
    }

    aid(unit: Player, aid: Zone) {
        if (unit.healthIsFull()) return;
        unit.heal();
        aid.destroy();
    }

    heal() {
        this.health += 5000;
        if (this.health > this.getMaxHealth()) {
            this.health = this.getMaxHealth();
        }
    }

    healthIsFull(): boolean {
        return this.health >= this.getMaxHealth();
    }

    extra(unit: Player, extra: Zone) {
        unit.bullets = unit.scene.bulletsExtra;
        extra.destroy();
    }

    aiLayer(bullet: Bullet, ai: Tile) {
        ai.destroy();
        let layer = ai.tilemapLayer;
        let explosive = bullet.scene.add.sprite(ai.getCenterX(), ai.getCenterY(), 'explosion');
        if ((<DynamicTilemapLayer>layer).removeTileAt) {
            (<DynamicTilemapLayer>layer).removeTileAt(ai.x, ai.y);
        }
        
        explosive.play('explosive');
        bullet.scene.soundBoom.play();
        
        bullet.destroy();
        let rect = bullet.scene.aiRect;
        let aiCount = layer.getTilesWithinWorldXY(rect.x, rect.y, rect.width, rect.height, {
            isNotEmpty: true
        }).length;
        
        if (aiCount === 0) {
            bullet.scene.win();
        }
    }
}