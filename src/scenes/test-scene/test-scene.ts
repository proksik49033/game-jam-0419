import * as Phaser from 'phaser';
import BaseSound = Phaser.Sound.BaseSound;
import WebGLPipeline = Phaser.Renderer.WebGL.WebGLPipeline;
import Text = Phaser.GameObjects.Text;
import GameObject = Phaser.GameObjects.GameObject;
import Tilemap = Phaser.Tilemaps.Tilemap;
import CursorKeys = Phaser.Types.Input.Keyboard.CursorKeys;
import StaticTilemapLayer = Phaser.Tilemaps.StaticTilemapLayer;
import DynamicTilemapLayer = Phaser.Tilemaps.DynamicTilemapLayer;
import PhysicsGroup = Phaser.Physics.Arcade.Group;
import Zone = Phaser.GameObjects.Zone;
import WebGLRenderer = Phaser.Renderer.WebGL.WebGLRenderer;
import { MapPoint, MapZone } from '../../types';

import tileset from './assets/intbuilding7a.png';
import track1 from './audio/music/htrack1-1.ogg';
import boom from './audio/boom.ogg';
import death from './audio/death.ogg';
import electro from './audio/electro.ogg';
import overheating from './audio/overheating.ogg';
import shoot from './audio/shoot.ogg';
import walk from './audio/walk.ogg';
import wzhh from './audio/wzhh.ogg';
import minigun_bullet from './assets/minigun_bullet.png';
import minigun_extra_bullet from './assets/minigun_extra_bullet.png';
import minigun_flash from './assets/minigun_flash.png';
import minigun_extra_flash from './assets/minigun_extra_flash.png';
import tilemap from './assets/map.json';
import Bullet from './Bullet';
import BulletExtra from './BulletExtra';
import enemy from './assets/enemy.png';
import Player from './Player';
import Turel from './Turel';
import mech_walk_spritesheet from './assets/mech_walk_spritesheet.png';
import mech_walk_spritesheet_json from './assets/mech_walk_spritesheet.json';
import Soldier from './Soldier';
import mech_fire_spritesheet from './assets/mech_fire_spritesheet.png';
import mech_fire_spritesheet_json from './assets/mech_fire_spritesheet.json';
import soldier_spritesheet from './assets/soldier_spritesheet.png';
import soldier_spritesheet_json from './assets/soldier_spritesheet.json';
import turret_spritesheet from './assets/turret_spritesheet.png';
import turret_spriteheet_json from './assets/turret_spritesheet.json';
import explosion_spriteset from './assets/example_explosion.png';
import blood_spriteset from './assets/bloodsplat3_strip15.png';
import electric_explosion from './assets/electric_explosion.png';
import glitch_shader from './shaders/glitch.frag';
import CustomPipeline from './CustomPipeline';
import drop_spritesheet from './assets/drop_spritesheet.png';
import drop_spritesheet_json from './assets/drop_spritesheet.json';
import fontStyles from '../fontStyles';

let setCollideWorldBounds = (elem) => {
    elem.setCollideWorldBounds(true);
}

let getMapPoint = (map:Tilemap, name:string):MapPoint => {
    return map.findObject('objects', obj => obj.name === name);
}

let getMapPointList = (map:Tilemap, objName:string):MapPoint[] => {
    return map.getObjectLayer('objects').objects.filter(object => object.name === objName);
}

let getMapZone = (map:Tilemap, name:string):MapZone => {
    return map.findObject('objects', obj => obj.name === name);
}




const TIME_TO_PLAY = 240000;

export default class TestScene extends Phaser.Scene {
    public music:BaseSound;
    public glitchShader:WebGLPipeline;
    public soundBoom:BaseSound;
    public soundWzhh:BaseSound;
    public soundShoot:BaseSound;
    public soundWalk:BaseSound;
    public soundDeath:BaseSound;
    public soundElectro:BaseSound;
    public soundOverheating:BaseSound;
    public timeRemain:number;
    public timeRemainDisplay:Text;
    public aiRect:MapZone;
    public enemies2SpawnPoints:MapPoint[];
    public soldiers1SpawnPoints:MapPoint[];
    public soldiers2SpawnPoints:MapPoint[];
    public player:Player;
    public cursors:CursorKeys;
    public map:StaticTilemapLayer;
    public aiLayer:DynamicTilemapLayer;
    public keys;
    public bullets:PhysicsGroup;
    public bulletsExtra:PhysicsGroup;
    public enemyBullets:PhysicsGroup;
    public enemies:PhysicsGroup;
    public soldiers:PhysicsGroup;
    public aids:PhysicsGroup;
    public extra:PhysicsGroup;
    public firstElectroZone:Zone;
    public trigger1:Zone;
    public trigger2:Zone;
    public finalTrigger:Zone;
    public startTime:number;
    constructor() {
        super('TestScene');
    }

    preload() {        
        var progress = this.add.graphics();

        this.load.on('progress', function (value) {

            progress.clear();
            progress.fillStyle(0xffffff, 1);
            progress.fillRect(0, 270, 800 * value, 60);

        });

        this.load.on('complete', function () {

            progress.destroy();

        });
        
        this.load.image('tiles', tileset);
        this.load.atlas('mech_walk', mech_walk_spritesheet, mech_walk_spritesheet_json);
        this.load.atlas('mech_fire', mech_fire_spritesheet, mech_fire_spritesheet_json);
        this.load.image('bullet', minigun_bullet);
        this.load.image('bullet_extra', minigun_extra_bullet);
        this.load.spritesheet('minigun_flash', minigun_flash, {
            frameWidth: 384,
            frameHeight: 221
        });
        this.load.spritesheet('minigun_extra_flash', minigun_extra_flash, {
            frameWidth: 384,
            frameHeight: 221
        });
        this.load.image('enemy', enemy);
        this.load.atlas('soldier', soldier_spritesheet, soldier_spritesheet_json);
        this.load.atlas('turret', turret_spritesheet, turret_spriteheet_json);
        this.load.tilemapTiledJSON('map', tilemap);
        this.load.spritesheet('explosion', explosion_spriteset, {
            frameWidth: 24,
            frameHeight: 24
        });
        this.load.spritesheet('electric_explosion', electric_explosion, {
            frameWidth: 256,
            frameHeight: 256
        });
        this.load.spritesheet('blood', blood_spriteset, {
            frameWidth: 480,
            frameHeight: 480
        });
        this.load.atlas('drop', drop_spritesheet, drop_spritesheet_json);
        this.load.audio('music', track1);
        this.load.audio('boom', boom);
        this.load.audio('death', death);
        this.load.audio('electro', electro);
        this.load.audio('overheating', overheating);
        this.load.audio('shoot', shoot);
        this.load.audio('walk', walk);
        this.load.audio('wzhh', wzhh);
        let renderer = this.game.renderer;
        if ((<WebGLRenderer>renderer).addPipeline) {
            this.glitchShader = (<WebGLRenderer>renderer).addPipeline(
                'glitch',
                new CustomPipeline(this.game, glitch_shader)
            );
        }
        
        this.glitchShader.setFloat2('u_canvas_size', +this.game.config.width, +this.game.config.height);
    }

    create() {
        
        
        if (!this.music) {
            this.music = this.sound.add('music');
        }        
        if (!this.music.isPlaying) {
            this.music.play();
        }
        this.soundBoom = this.sound.add('boom');
        this.soundDeath = this.sound.add('death');
        this.soundElectro = this.sound.add('electro');
        this.soundOverheating = this.sound.add('overheating');
        this.soundShoot = this.sound.add('shoot');
        this.soundWalk = this.sound.add('walk', {
            loop: true,
            rate: 2,
            detune: -100,
            volume: 0.2
        });
        this.soundWzhh = this.sound.add('wzhh');
        

        this.input.on(
            'pointerdown',
            () => {
                this.scale.startFullscreen();
            },
            this
        );

        this.timeRemain = TIME_TO_PLAY;
        this.timeRemainDisplay = this
            .add
            .text(0, 0, `Time remain: ${(this.timeRemain / 1000).toFixed(0)} s`, fontStyles)            
            .setDepth(100)
            .setScrollFactor(0);
        this.timeRemainDisplay.setPosition(
            +this.game.config.width / 2 - this.timeRemainDisplay.width / 2,
            5
        );
        
        
        const map = this.make.tilemap({ key: 'map' });
        const tiles = map.addTilesetImage('intbuilding7a', 'tiles', 32, 32, 3, 6);
        const spawnPoint = getMapPoint(map, 'spawn');
        this.aiRect = getMapZone(map, 'ai_rect');
        console.log(this.aiRect);
        const enemiesSpawnPoints = getMapPointList(map, 'enemy');
        console.log(enemiesSpawnPoints);
        this.enemies2SpawnPoints = getMapPointList(map, 'enemy2');
        const soldiersSpawnPoints = getMapPointList(map, 'soldier');
        this.soldiers1SpawnPoints = getMapPointList(map, 'soldier1');
        this.soldiers2SpawnPoints = getMapPointList(map, 'soldier2');
        
        const electroPoints = getMapPointList(map, 'electro');
        const aidPoints = getMapPointList(map, 'aid');
        const extraPoints = getMapPointList(map, 'extra');
        const firstElectroPoint = getMapZone(map, 'first_electro');
        const trigger1 = getMapZone(map, 'trigger1');
        const trigger2 = getMapZone(map, 'trigger2');
        const finalTrigger = getMapZone(map, 'final_trigger');
        

        
        this.cursors = this.input.keyboard.createCursorKeys();

        this.map = map.createStaticLayer('map', tiles);
        this.aiLayer = map.createDynamicLayer('ai', tiles, 0, 0);
        this.map.setCollisionByProperty({'isColliding': true});
        this.aiLayer.setCollisionByProperty({'isColliding': true});

        

        this.keys = {};

        this.keys.a = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.A);
        this.keys.d = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.D);
        this.keys.space = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.SPACE);

        

        this.bullets = this.physics.add.group({
            classType: Bullet,
            runChildUpdate: true,
        });
        this.bulletsExtra = this.physics.add.group({
            classType: BulletExtra,
            runChildUpdate: true,
            
        });

        this.enemyBullets = this.physics.add.group({
            classType: Bullet,
            runChildUpdate: true,
            
        });

        let camera = this.cameras.main;
        camera.setBounds(0, 0, map.widthInPixels, map.heightInPixels);
        camera.setRenderToTexture(this.glitchShader);
        camera.setPipeline('glitch');
        

        this.physics.world.setBounds(0, 0, map.widthInPixels, map.heightInPixels);

        
        
        this.player = new Player(this, spawnPoint.x, spawnPoint.y);
        
        this.physics.add.existing(this.player);
        this.player.setBullets(this.bullets);
        camera.startFollow(this.player);

        this.createAnims();        

        this.enemies = this.physics.add.group({
            defaultKey: 'turret',
            classType: Turel,
            runChildUpdate: true,
            immovable: true,            
        });

        this.soldiers = this.physics.add.group({
            defaultKey: 'soldier',
            classType: Soldier,
            runChildUpdate: true
        });

        this.aids = this.physics.add.group({
            defaultKey: 'drop',
            defaultFrame: 'health'
        });

        this.extra = this.physics.add.group({
            defaultKey: 'drop',
            defaultFrame: 'bullet'
        });

        this.addEnemies(enemiesSpawnPoints);
        
        this.addSoldiers(soldiersSpawnPoints);

        aidPoints.forEach(element => {            
            this
                .aids
                .get()
                .setScale(2.5)
                .setPosition(element.x, element.y);
        });

        extraPoints.forEach(element => {
            this
                .extra
                .get()
                .setScale(2.5)
                .setPosition(element.x, element.y);
        });

        electroPoints.forEach(element => {
            let zone = this.add.zone(element.x, element.y, 1, 1);
            this.physics.world.enable(zone);
            this.physics.add.overlap(zone, this.player, this.hitElectro);
        });

        this.firstElectroZone = this.getZoneFromTrigger(firstElectroPoint);
        
        this.trigger1 = this.getZoneFromTrigger(trigger1);
        this.trigger2 = this.getZoneFromTrigger(trigger2);
        this.finalTrigger = this.getZoneFromTrigger(finalTrigger);
        
        this.enableZone(this.firstElectroZone, this.hitElectro);        
        this.enableZone(this.trigger1, this.onTrigger1);
        this.enableZone(this.trigger2, this.onTrigger2);
        this.enableZone(this.finalTrigger, this.onFinalTrigger);

        

        this.physics.add.collider(this.player, this.enemies);
        
        this.physics.add.collider(this.player, this.map);
        this.physics.add.collider(this.soldiers, this.soldiers);
        this.physics.add.collider(this.soldiers, this.map);
        

        this.physics.add.overlap(this.enemyBullets, this.player, Player.prototype.hit);        
        this.physics.add.overlap(this.enemies, this.bullets, Turel.prototype.hit);
        this.physics.add.overlap(this.enemies, this.bulletsExtra, Turel.prototype.hit);
        this.physics.add.overlap(this.bullets, this.soldiers, Soldier.prototype.hit);
        this.physics.add.overlap(this.bulletsExtra, this.soldiers, Soldier.prototype.hit);
        this.physics.add.overlap(this.player, this.soldiers, Soldier.prototype.dead);
        this.physics.add.overlap(this.player, this.aids, this.player.aid);
        this.physics.add.overlap(this.player, this.extra, this.player.extra);

        // this.enemiesDisplay = this.add.text(5, 55, 'Enemies: 0', fontStyles).setScrollFactor(0);        
        // this.fpsDisplay = this.add.text(5, 80, 'FPS', fontStyles).setScrollFactor(0);
        
        this.startTime = this.time.now;
    }

    update(time:number, delta:number) {
        this.timeRemain -= delta;
        if (this.timeRemain < 0) {
            this.gameOver('time is up');
        }
        this.timeRemainDisplay.setText(`Time remain: ${(this.timeRemain / 1000).toFixed(0)} s`);
        this.player.update(time, delta);
        var enemiesCount = this.enemies.countActive() + this.soldiers.countActive();
        //this.enemiesDisplay.setText('Enemies: ' + enemiesCount);
        //this.fpsDisplay.setText('Fps: ' + this.game.loop.actualFps);
        this.glitchShader.setFloat1('time', time);
    }

    hitEnemy(enemy:Soldier, bullet:Bullet) {        
        enemy.setActive(false);
        enemy.setVisible(false);        
        bullet.destroy();
    }

    createAnims() {
        this.anims.create({
            key: 'minigun_extra.shoot',
            frames: [{
                key: 'minigun_extra_flash',
                frame: 0
            }],
            duration: 10,
            hideOnComplete: true
        });
        this.anims.create({
            key: 'minigun.shoot',
            frames: [{
                key: 'minigun_flash',
                frame: 0
            }],
            duration: 10,
            hideOnComplete: true
        });
        this.anims.create({
            key: 'bloodsplat',
            frames: this.anims.generateFrameNumbers('blood', {
                start: 0,
                end: 14 
            }),
            hideOnComplete: true
        });

        this.anims.create({
            key: 'explosive.electric',
            frames: this.anims.generateFrameNumbers('electric_explosion', {
                start: 0,
                end: 16
            }),
            hideOnComplete: true
        });

        this.anims.create({
            key: 'explosive',
            frames: this.anims.generateFrameNumbers('explosion', {
                start: 0, 
                end: 3
            }),
            hideOnComplete: true
        });

        this.anims.create({
            key: 'mech.walk',
            frames: this.anims.generateFrameNames('mech_walk'),
            repeat: -1
        });
        this.anims.create({
            key: 'mech.stay',
            frames: [{
                key: 'mech_walk',
                frame: 'frame1'
            }]            
        });

        this.anims.create({
            key: 'mech.idle',
            frames: [{
                key: 'mech_fire',
                frame: 'frame04'
            }]
        });

        this.anims.create({
            key: 'mech.fire',
            frames: this.anims.generateFrameNames('mech_fire'),
        });

        this.createSoldierAnims();
        this.createTurretAnims();
    }

    createSoldierAnims() {
        this.createSoldierIdleAnim();
        this.createSoldierFireAnim();
        this.createSoldierWalkAnim();
    }

    createSoldierIdleAnim() {        
        this.anims.create({
            key: 'soldier.idle',
            frames: this.anims.generateFrameNames('soldier', {
                prefix: 'idle',
                start: 0,
                end: 19,
                zeroPad: 4
            })
        });
    }

    createSoldierFireAnim() {        
        this.anims.create({
            key: 'soldier.fire',
            frames: this.anims.generateFrameNames('soldier', {
                prefix: 'fire',
                start: 0,
                end: 2,
                zeroPad: 4
            })
        });
    }

    createSoldierWalkAnim() {        
        this.anims.create({
            key: 'soldier.walk',
            frames: this.anims.generateFrameNames('soldier', {
                prefix: 'walk',
                start: 0,
                end: 19,
                zeroPad: 4
            })
        });
    }

    createTurretAnims() {
        this.anims.create({
            key: 'turret.idle',
            frames: [{
                key: 'turret',
                frame: 'fire0000'
            }]
        });

        this.createTurretFireAnim();
    }

    createTurretFireAnim() {
        this.anims.create({
            key: 'turret.fire',
            frames: this.anims.generateFrameNames('turret', {
                prefix: 'fire',
                start: 0,
                end: 7,
                zeroPad: 4
            }),
            duration: 10
        });
    }

    hitElectro(electro:Zone, player:Player) {
        electro.destroy();
        player.hitElectro();
    }

    win() {        
        this.gameOver(`You win`);        
    }

    gameOver(message:string) {
        this.soundWalk.stop();        
        this.scene.start('GameOver', {message: message});
    }

    onTrigger1(trigger:Zone, player:Player) {
        let scene = player.scene;        
        scene.addSoldiers(scene.soldiers1SpawnPoints);
        trigger.destroy();
    }

    onTrigger2(trigger:Zone, player:Player) {
        let scene = player.scene;
        scene.addSoldiers(scene.soldiers2SpawnPoints);
        scene.addEnemies(scene.enemies2SpawnPoints);
        trigger.destroy();
    }

    onFinalTrigger(trigger:Zone, player:Player) {
        let scene = player.scene;
        scene.physics.add.collider(scene.bullets, scene.aiLayer, scene.player.aiLayer);
        scene.physics.add.collider(scene.bulletsExtra, scene.aiLayer, scene.player.aiLayer);
        trigger.destroy();
    }

    addEnemies(spawn:MapPoint[]) {
        spawn.forEach(element => {
            let enemy = this.enemies.get();
            enemy.setPosition(element.x, element.y);
            enemy.setBullets(this.enemyBullets);
            enemy.setScale(0.2);
            setCollideWorldBounds(enemy);
        });
    }

    addSoldiers(spawn:MapPoint[]) {
        
        spawn.forEach(element => {
            let soldier = this.soldiers.get();
            soldier.setPosition(element.x, element.y);
            soldier.setBullets(this.enemyBullets);
            soldier.setScale(0.1);
            setCollideWorldBounds(soldier);
        });
    }

    getZoneFromTrigger(trigger:MapZone):Zone {
        return this.add.zone(
            trigger.x,
            trigger.y,
            0,
            0
        ).setSize(
            trigger.width,
            trigger.height
        );
    }

    enableZone(zone:Zone, callback:ArcadePhysicsCallback) {
        this.physics.world.enable(zone);
        (<Body>zone.body).debugShowBody = true;
        zone.body.debugBodyColor = 0xff0000;
        this.physics.add.overlap(zone, this.player, callback);
    }
}