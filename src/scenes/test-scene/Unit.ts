import * as Phaser  from 'phaser';
import PhysicsGroup = Phaser.Physics.Arcade.Group;
import {Gun} from '../../types';
import Bullet from './Bullet';
import TestScene from './test-scene';
const FIRE_RATE = 70;

export default class Unit extends Phaser.Physics.Arcade.Sprite {
    public nextFire:number;
    public health:number;
    public bullets: PhysicsGroup;
    public scene: TestScene;
    constructor(scene: TestScene, x:number, y:number, sprite: string, frame?: string) {
        super(scene, x, y, sprite, frame);
        this.nextFire = 0;
        this.health = this.getMaxHealth();
    }

    setBullets(bullets: PhysicsGroup) {
        this.bullets = bullets;
    }

    damage(points: number) {
        this.health -= points;
    }

    isAlive(): boolean {
        return this.health > 0;
    }

    isNear(): boolean {
        return this.active && this.distanceToPlayer() < this.watchDistance();
    }

    isInCamera() {
        let rect = this.scene.cameras.main.worldView;
        return Phaser.Geom.Rectangle.ContainsPoint(rect, <Phaser.Geom.Point>{
            x: this.x,
            y: this.y
        });
    }

    distanceToPlayer() {
        return Phaser.Math.Distance.Between(this.x, this.y, this.scene.player.x, this.scene.player.y);
    }

    watchDistance() {
        return 500;
    }

    getWeapon(): Gun[] {
        return [{
            x: 0,
            y: 0,
            rotation: 0
        }];
    }

    fire(time:number, delta:number) {
        if (this.scene.time.now > this.nextFire) {
            this.nextFire = this.scene.time.now + FIRE_RATE;
            var guns = this.getWeapon();
            guns.forEach(gun => {
                var bullet = this.bullets.get();
                
                if (bullet) {
                    bullet.activate();                

                    bullet.fire(gun);
                }            
            });
        }
    }

    getMaxHealth() {
        return 10;
    }    

    hit(unit: Unit, bullet: Bullet) {        
        unit.damage(bullet.damage);
        bullet.destroy();
        if (!unit.isAlive()) {
            unit.destroy();
        }
                
    }

    update(time: number, delta: number) {
        super.update(time, delta);
        if (this.isInCamera()) {
            this.scene.physics.world.enable(this);
        } else {
            this.scene.physics.world.disable(this);
        }
    }
}