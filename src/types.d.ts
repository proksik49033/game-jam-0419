export interface MapPoint {
    x: number,
    y: number,
    name: string
}

export interface MapZone extends MapPoint {
    width: number,
    height: number
}

export interface Gun {
    x: number,
    y: number,
    rotation: number
}

export interface TransformationInfo {
    translateX: number,
    translateY: number,
    rotation: number,
    scaleX: number,
    scaleY: number
}

