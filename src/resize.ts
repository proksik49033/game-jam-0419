export default class Resize {
    public canvas: HTMLElement;
    public window: Window;
    constructor(public game:Phaser.Game) {        
        this.resize = this.resize.bind(this);
        this.canvas = <HTMLCanvasElement>document.querySelector("canvas");
        this.window = window;
    }

    resize() {        
        var windowWidth = this.window.innerWidth;
        var windowHeight = this.window.innerHeight;
        var windowRatio = windowWidth / windowHeight;
        var gameRatio = +this.game.config.width / +this.game.config.height;
        if(windowRatio < gameRatio){
            this.canvas.style.width = windowWidth + "px";
            this.canvas.style.height = (windowWidth / gameRatio) + "px";
        }
        else{
            this.canvas.style.width = (windowHeight * gameRatio) + "px";
            this.canvas.style.height = windowHeight + "px";
        }
    }
}